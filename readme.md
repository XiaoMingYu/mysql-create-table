## sffi/recovery 使用教程

#### 插件介绍:

该插件用于对数据库新增表结构,功能单一

#### 使用教程:

```php
$config = [
    'host' => '127.0.0.1',
    'username'=>'root',
    'password'=>'root',
    'database'=>'test',
    'port'=>'3306'
];

//根据json文件创建数据库表
\recovery\Factory::parseJson('/your/json/file/path', $config)->exe();
//根据php数组创建数据库表
\recovery\Factory::parseArray($data, $config)->exe();
//根据sql文件创建数据库表
\recovery\Factory::parseMysql('./test.sql', $config)->exe();
//根据yaml文件创建数据库表
\recovery\Factory::parseYaml('./test.sql', $config)->exe();
```

如果仅希望获取sql语句,可调整为如下代码:

```php
$sql = \recovery\Factory::parseArray($data, [])->getSql();
```

请确保php数组,json,yaml,ini等文件格式符合如下结构:

```json
[
  {
    // 表名称
    "table_name": "",
    // 引擎
    "engine": "InnoDB",
    // 字段
    "fields": [
      {
        // 字段名称
        "name": "",
        // 字段类型
        "type": "",
        // 字段长度
        "length": "",
        // 是否自增长
        "is_auto_increment": false,
        // 特征
        "features": "UNSIGNED",
        // 是否非空
        "is_not_null": true,
        // 默认值
        "default": "",
        // 字段备注
        "remark": ""
      }
    ],
    // 索引
    "indexs": [
      {
        // 索引名称
        "name": "",
        // 索引字段,多字段用英文逗号分割
        "field": "",
        // 索引类型: FULLTEXT|NORMAL|SPATIAL|UNIQUE
        "type": "FULLTEXT",
        // 索引方式: BTREE|HASH
        "method": "HASH"
      }
    ]
  }
]
```