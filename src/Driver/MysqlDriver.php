<?php

namespace sffi\recovery\Driver;


class MysqlDriver extends BaseDriver
{
    /**
     * 解析sql文件路径
     * @param string $path sql文件所在位置
     */
    public function parse(...$args)
    {
        list($path) = $args;
        $this->checkFile($path);
        $content = file_get_contents($path);
        $this->checkMysqlIncludeDelete($content);
        $this->sql = $content;
        return $this;
    }

    /**
    * 检查文件格式与路径
    * @param string $path 路径
     */
    public function checkFile(string $path)
    {
        if (!file_exists($path)) {
            throw new \Exception('文件不存在');
        }
        $pathInfo = pathinfo($path);
        if ($pathInfo['extension'] != 'sql') {
            throw new \Exception('非sql文件');
        }
        return true;
    }
}