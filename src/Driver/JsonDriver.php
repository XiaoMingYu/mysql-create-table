<?php

namespace sffi\recovery\Driver;

use sffi\recovery\Exception\FileNotException;
use sffi\recovery\Factory;

/**
 * JSON文件驱动
 */
class JsonDriver extends BaseDriver
{
    /**
     * 解析JSON文件路径
     * @param mixed ...$args
     * @throws FileNotException
     */
    public function parse(...$args)
    {
        list($path) = $args;
        $this->checkFile($path);
        $content = file_get_contents($path);
        $arr = json_decode($content);
        return Factory::parseArray($arr, $this->config);
    }

    /**
     * 检查文件格式与路径
     * @param string $path 路径
     */
    public function checkFile(string $path): bool
    {
        if (!file_exists($path)) {
            throw new FileNotException();
        }
        $pathInfo = pathinfo($path);
        if ($pathInfo['extension'] != 'json') {
            throw new \Exception('非sql文件');
        }
        return true;
    }
}