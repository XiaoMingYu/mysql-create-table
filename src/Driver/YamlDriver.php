<?php

namespace sffi\recovery\Driver;

use sffi\recovery\Factory;
use Symfony\Component\Yaml\Yaml;

/**
 * Yaml文件驱动
 */
class YamlDriver extends BaseDriver
{
    /**
     * 解析yaml文件路径
     */
    public function parse(...$args)
    {
        list($path) = $args;
        $data = Yaml::parseFile($path);
        return Factory::parseArray($data, $this->config);
    }
}