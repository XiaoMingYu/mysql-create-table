<?php

namespace sffi\recovery\Driver;

use sffi\recovery\Core\TableEntity;
/**
 * 数组驱动
 */
class ArrayDriver extends BaseDriver
{
    /**
     * @param array $array mysql对应数组
     */
    public function parse(...$arg): self
    {
        list($tables) = $arg;
        $jsonStr = json_encode($tables);
        $this->checkMysqlIncludeDelete($jsonStr);
        foreach ($tables as $table) {
            $te = new TableEntity($this->config['prefix'].$table['table_name']);
            foreach ($table['fields'] as $field) {
                $te->addField($field['name'], $field['type'], (int)($field['length']??0), $field['is_auto_increment']??false, $field['default']??'', $field['key']??false, $field['remark']??'');
            }
            foreach ($table['indexs'] as $index) {
                $te->addIndex($index['name'], $index['field'], $index['type'], $index['method']);
            }
            $te->setEngine($table['engine']);
            $this->addTable($te);
        }
        $this->sql = $this->generalSql();
        return $this;
    }
}