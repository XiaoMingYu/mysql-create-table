<?php
namespace sffi\recovery\Constant;

class FieldConstant{
    const TYPE_FIELD_INT = 'int';
    const TYPE_FIELD_BIGINT = 'bigint';
    const TYPE_FIELD_BINARY = 'binary';
    const TYPE_FIELD_BIT = 'bit';
    const TYPE_FIELD_CHAR = 'char';
    const TYPE_FIELD_DATE = 'date';
    const TYPE_FIELD_DATETIME = 'datetime';
    const TYPE_FIELD_DECIMAL = 'decimal';
    const TYPE_FIELD_DOUBLE = 'double';
    const TYPE_FIELD_ENUM = 'enum';
    const TYPE_FIELD_FLOAT = 'float';
    const TYPE_FIELD_INTEGER = 'integer';
    const TYPE_FIELD_JSON = 'json';
    const TYPE_FIELD_LINESTRING = 'linestring';
    const TYPE_FIELD_LONGTEXT = 'longtext';
    const TYPE_FIELD_MEDIUMTEXT = 'mediumtext';
    const TYPE_FIELD_MEDIUMINT = 'mediumint';
    const TYPE_FIELD_MEDIUMBLOB = 'mediumblob';
    const TYPE_FIELD_MULTILINESTRING = 'multilinestring';
    const TYPE_FIELD_MULTIPOINT = 'multipoint';
    const TYPE_FIELD_NUMERIC = 'numeric';
    const TYPE_FIELD_POINT = 'point';
    const TYPE_FIELD_SMALLINT = 'smallint';
    const TYPE_FIELD_TEXT = 'text';
    const TYPE_FIELD_TIME = 'time';
    const TYPE_FIELD_TIMESTAMP = 'timestamp';
    const TYPE_FIELD_TINYINT = 'tinyint';
    const TYPE_FIELD_TINYTEXT = 'tinytext';
    const TYPE_FIELD_VARBINARY = 'varbinary';
    const TYPE_FIELD_VARCHAR = 'varchar';
    const TYPE_FIELD_YEAR = 'year';
    const TYPE_FIELD_REAL = 'real';
    const TYPE_FIELD_SET = 'set';
}