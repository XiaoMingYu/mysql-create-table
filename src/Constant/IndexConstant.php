<?php
namespace sffi\recovery\Constant;

class IndexConstant{
    const TYPE_INDEX_UNIQUE = 'UNIQUE';
    const TYPE_INDEX_FULLTEXT = 'FULLTEXT';
    const TYPE_INDEX_NORMAL = 'NORMAL';
    const TYPE_INDEX_SPATIAL = 'SPATIAL';

    const TYPE_INDEX_HASH = 'HASH';
    const TYPE_INDEX_BTREE = 'BTREE';
}