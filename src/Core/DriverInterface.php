<?php

namespace sffi\recovery\Core;

interface DriverInterface
{
    /**
     * 解析方法
     */
    public function parse();

    /**
     * 执行方法
     */
    public function exe();
}