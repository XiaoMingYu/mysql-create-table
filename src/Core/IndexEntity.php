<?php

namespace sffi\recovery\Core;

use sffi\recovery\Constant\IndexConstant;
use sffi\recovery\Exception\FileNotException;

class IndexEntity
{
    protected string $name;
    protected array $fields;
    protected string $type;// from IndexConstant
    protected string $method;// from IndexConstant
    protected string $remark;

    /**
     * 初始化索引
     * @param string $name 索引名称
     * @param string $field 索引字段
     * @param string $type 索引类型
     * @param string $method 索引方式
     * @param string $remark 索引说明/备注
     */
    public function __construct(string $name, string $field, string $type, string $method, string $remark = '')
    {
        $this->name = $name;
        $this->setField($field);
        $this->setType($type);
        $this->setMethod($method);
        $this->setRemark($remark);
    }

    /**
     * 获取索引名称
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * 设置索引名称
     * @param string $name
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 获取字段
     * @return array
     */
    public function getField(): array
    {
        return $this->fields;
    }

    /**
     * @param string $field
     */
    public function setField(string $field): self
    {
        $filedArr = explode(',', $field);
        $this->fields = array_unique(array_merge($this->fields, $filedArr));
        $this->fields[] = $field;
        return $this;
    }

    /**
     * 获取索引类型
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * 设置索引类型
     * @param string $type
     */
    public function setType(string $type): self
    {
        // 判断索引常量中是否存在该索引类型
        if (!defined("\\sffi\\recovery\\Constant\\IndexConstant::TYPE_INDEX_" . strtoupper($type))) {
            throw new FileNotException('该索引类型不存在');
        }
        $this->type = $type;
        return $this;
    }

    /**
     * 获取索引方式
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * 设置索引方式
     * @param string $method 索引方式
     */
    public function setMethod(string $method): self
    {
        if (!defined("\\sffi\\recovery\\Constant\\IndexConstant::TYPE_INDEX_" . strtoupper($method))) {
            throw new FileNotException('该索引方式不存在');
        }
        $this->method = $method;
        return $this;
    }

    /**
     * 获取索引备注
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): self
    {
        $this->remark = $remark;
        return $this;
    }

    /**
     * 生成mysql语句
     */
    public function generalSql()
    {
        return sprintf("%s INDEX `%s`(%s) USING %s COMMENT '%s',", $this->type == IndexConstant::TYPE_INDEX_NORMAL ? '' : $this->type, $this->name, '`' . implode('`,`', $this->fields) . '`', $this->method, $this->remark);
    }
}