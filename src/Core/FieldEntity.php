<?php

namespace sffi\recovery\Core;

class FieldEntity 
{
    protected string $name;
    protected string $type;
    protected int $length;
    protected $default;
    protected $features;
    protected bool $isAutoIncrement;
    protected string $remark;
    protected bool $key;

    /**
     * 初始化字段
     * @param string $name 字段名称
     * @param string $type 字段类型
     * @param int $length 字段长度
     * @param string|int|null $default 字段默认值
     * @param string $remark 字段说明/备注
     */
    public function __construct(string $name, string $type, int $length, $default, string $remark = '')
    {
        $this->name = $name;
        $this->type = $type;
        $this->length = $length;
        $this->default = $default;
        $this->remark = $remark;
    }

    /**
     * 获取字段名称
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 设置字段名称
     * @param mixed $name
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * 获取字段类型
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 设置字段类型
     * @param mixed $type
     */
    public function setType($type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * 获取字段长度
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * 设置字段长度
     * @param mixed $length
     */
    public function setLength($length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     * 获取默认值
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * 设置默认值
     * @param mixed $default
     */
    public function setDefault($default): self
    {
        $this->default = $default;
        return $this;
    }

    /**
     * 获取特征
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * 设置特征
     * @param mixed $features
     */
    public function setFeatures($features): self
    {
        $this->features = $features;
        return $this;
    }

    /**
     * 获取备注
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * 设置备注
     * @param string $remark
     */
    public function setRemark(string $remark): self
    {
        $this->remark = $remark;
        return $this;
    }

    /**
     * 获取增长
     * @return mixed
     */
    public function getIsAutoIncrement()
    {
        return $this->isAutoIncrement;
    }

    /**
     * 设置自增长
     * @param mixed $isAutoIncrement
     */
    public function setIsAutoIncrement($isAutoIncrement): self
    {
        $this->isAutoIncrement = $isAutoIncrement;
        return $this;
    }

    /**
     * 获取是否主键
     * @return bool
     */
    public function isKey(): bool
    {
        return $this->key;
    }

    /**
     * 设置是否主键
     * @param bool $key
     */
    public function setKey(bool $key): self
    {
        $this->key = $key;
        return $this;
    }

    /**
     * 生成mysql语句
     */
    public function generalSql()
    {
        $sql = sprintf('`%s` %s(%d) %s %s %s %s COMMENT \'%s\',', $this->name,
            $this->type,
            $this->length,
            $this->features,
            $this->default === null ? 'NULL' : 'NOT NULL',
            $this->default ? 'DEFAULT ' . $this->default : '',
            $this->isAutoIncrement ? 'AUTO_INCREMENT' : '',
            $this->remark
        );
        if ($this->key) {
            $sql .= sprintf('PRIMARY KEY (`%s`),', $this->name);
        }
        return $sql;
    }
}