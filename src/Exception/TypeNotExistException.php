<?php
namespace sffi\recovery\Exception;

class TypeNotExistException extends \Exception{
    protected $code = 3001;
    protected $message = '该字段类型不存在';
}