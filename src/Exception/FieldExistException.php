<?php
namespace sffi\recovery\Exception;

class FieldExistException extends \Exception{
    protected $code = 3001;
    protected $message = '该字段已存在';
}