<?php
namespace sffi\recovery\Exception;

class EngineTypeException extends \Exception {
    protected $code = 4001;
    protected $message = '不支持该引擎模式';
}