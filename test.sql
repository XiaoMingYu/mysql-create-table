CREATE TABLE `hero` (
    `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增长ID',
    PRIMARY KEY (`id`),
    `name` varchar(32) NOT NULL DEFAULT '' COMMENT '英雄名称',
    `update_time` datetime null comment "更新时间"
) ENGINE=InnoDB;
