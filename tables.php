<?php
return [
    [
        "table_name"=>"hero",
        "fields" => [
                [
                    'name'=>"id",
                    'remark' => '',
                    'type'=> 'int',
                    'is_auto_increment' => true,
                    'is_unsigned'=>true,
                    'is_not_null'=>true,
                    'default'=>0,
                    'length'=>10
                ]
        ],
        'index'=>[

        ]
    ]
];